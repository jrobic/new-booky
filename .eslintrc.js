module.exports = {
  root: true,
  parserOptions: {
    ecmaVersion: 2020,
    sourceType: 'module',
    ecmaFeatures: {
      jsx: true,
    },
  },
  extends: [
    'eslint:recommended',
    'plugin:import/warnings',
    'plugin:import/errors',
    'airbnb/base',
    'plugin:prettier/recommended',
    'prettier',
  ],
  env: {
    browser: true,
    node: true,
    'jest/globals': true,
  },
  plugins: ['jest', 'testing-library', 'prettier'],
  rules: {
    'prettier/prettier': ['error', { singleQuote: true }],
  },
  overrides: [
    {
      files: '**/*.+(ts|tsx)',
      parser: '@typescript-eslint/parser',
      parserOptions: {
        tsconfigRootDir: __dirname,
        project: './tsconfig.json',
      },

      extends: [
        'eslint:recommended',
        'plugin:import/warnings',
        'plugin:import/errors',
        'plugin:import/typescript',
        'plugin:jsx-a11y/recommended',
        'plugin:@typescript-eslint/recommended',
        'airbnb',
        'airbnb-typescript',
        'airbnb/hooks',
        'plugin:prettier/recommended',
        'prettier',
      ],
      plugins: ['unicorn'],
      rules: {
        'prettier/prettier': ['error', { singleQuote: true }],

        // 'import/order': 'off',
        'import/namespace': 'off',
        'import/prefer-default-export': 'off',
        'import/no-extraneous-dependencies': [
          'error',
          {
            devDependencies: ['**/vite.config.ts'],
          },
        ],
        'react/prop-types': 'off',
        'react/require-default-props': 'off',
        'react/jsx-props-no-spreading': 'off',
        'no-use-before-define': 'off',
        '@typescript-eslint/no-use-before-define': ['error', { variables: false }],
        'unicorn/better-regex': 'error',
        'unicorn/filename-case': [
          'error',
          {
            cases: {
              camelCase: true,
              pascalCase: true,
            },
          },
        ],
      },
    },
    {
      // Disable some rules in unit tests.
      files: ['**/*.+(test|spec).+(ts|tsx)'],
      rules: {
        'import/no-extraneous-dependencies': 'off',
        '@typescript-eslint/no-non-null-assertion': 'off',
        '@typescript-eslint/no-object-literal-type-assertion': 'off',
        '@typescript-eslint/no-var-requires': 'off',
        '@typescript-eslint/no-explicit-any': 'off',
        'global-require': 'off',
      },
    },
    {
      // Disable some rules in stories.
      files: ['**/*.stories.tsx'],
      rules: {
        'import/no-extraneous-dependencies': 'off',
        'import/no-anonymous-default-export': 'off',
      },
    },
    {
      // Disable some rules in unit tests.
      files: ['**/cypress/**/*.+(js|ts)', '**/*cy.spec.+(ts|tsx)'],
      plugins: ['eslint-plugin-cypress'],
      extends: ['plugin:cypress/recommended'],
      env: { 'cypress/globals': true, mocha: true },
      rules: {
        'import/no-extraneous-dependencies': 'off',
      },
    },
    {
      files: ['*.d.ts'],
      rules: {
        'unicorn/filename-case': 'off',
        'unicorn/prevent-abbreviations': 'off',
      },
    },
    {
      files: ['*.js'],
      rules: {
        'unicorn/filename-case': 'off',
        'unicorn/prevent-abbreviations': 'off',
        'import/no-extraneous-dependencies': [
          'error',
          {
            devDependencies: ['**/tailwind.config.js', 'config/**', 'scripts/**'],
          },
        ],
      },
    },
  ],
};
