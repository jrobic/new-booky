const packageName = require('./package.json').name.replace(/@\w+\//, '');
const server = require('../../config/test/jest.service')(packageName);

module.exports = {
  ...server,
  moduleNameMapper: {
    ...server.moduleNameMapper,
  },
};
