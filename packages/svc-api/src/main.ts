/* istanbul ignore file */

// eslint-disable-next-line @typescript-eslint/no-var-requires
import { startServer, StartServerResponse } from './server';
import config from './utils/config';

startServer().then(async ({ app }: StartServerResponse): Promise<void> => {
  const address = await app.listen(
    config.BOOKY_API_PORT || process.env.PORT || '3000',
    config.BOOKY_API_ADDRESS ? '0.0.0.0' : undefined,
  );

  if (process.env.NODE_ENV !== 'production') {
    app.log.info(`[GraphQL] ${address}/playground ${address}/altair`);
  }
});
