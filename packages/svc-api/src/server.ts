import Fastify, { FastifyInstance } from 'fastify';
import prettifier from '@mgcrea/pino-pretty-compact';
import fastifyRequestLogger from '@mgcrea/fastify-request-logger';
import healthCheck from 'fastify-healthcheck';

import { DeepPartial } from './types';
import config, { Config } from './utils/config';

export interface StartServerResponse {
  app: FastifyInstance;
}

interface Context {
  env: string;
  config: Partial<Config>;
  db: unknown;
}

export interface StartServer {
  db?: DeepPartial<Context['db']>;
}

export const startServer = async ({ db }: StartServer = {}): Promise<StartServerResponse> => {
  const context: Context = {
    env: 'development',
    config,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    db,
  };

  // eslint-disable-next-line no-console
  console.log({ context });

  const app = Fastify({
    logger: !config.isTest && {
      prettyPrint: config.NODE_ENV !== 'production',
      prettifier,
    },
  });

  app.register(fastifyRequestLogger);

  app.register(healthCheck, { exposeUptime: true });

  app.setErrorHandler((error, request, reply) => {
    // eslint-disable-next-line no-console
    app.log.error(error);

    reply.code(error.statusCode || 500).send(error);
  });

  await app.ready();

  return {
    app,
  };
};
