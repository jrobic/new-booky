import { createTestServer } from './tests';

describe('Health Routes', () => {
  it('should return health ', async () => {
    const { server } = await createTestServer();

    const res = await server.get('/health');

    expect(res.body).toEqual(
      expect.objectContaining({
        status: 'ok',
        statusCode: 200,
        uptime: expect.any(Number),
      }),
    );
  });
});
