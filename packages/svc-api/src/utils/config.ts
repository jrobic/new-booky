export interface Config {
  BOOKY_API_PORT: string;
  [k: `BOOKY_${string}`]: string;
  NODE_ENV: string;
  isTest: string;
}

const config: Partial<Config> = (process.env.config as unknown as Partial<Config>) || {};

export default config;
