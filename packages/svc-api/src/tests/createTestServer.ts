// eslint-disable-next-line import/no-extraneous-dependencies
import supertest from 'supertest';
import { StartServer, startServer } from '../server';

export type CreateTestServer = StartServer;

export async function createTestServer({ db }: CreateTestServer = {}): Promise<{
  server: supertest.SuperTest<supertest.Test>;
}> {
  const { app } = await startServer({ db });

  return {
    server: supertest(app.server),
  };
}
