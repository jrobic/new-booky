/* istanbul ignore file */

const fs = require('fs');
const path = require('path');

const { name } = require('../package.json');

const NODE_ENV = process.env.NODE_ENV || 'development';

const projectName = name.split('/')[0].replace('@', '');

module.exports = function getEnv() {
  const appDirectory = fs.realpathSync(process.cwd());
  const dotenvPath = path.resolve(appDirectory, '.env');

  // https://github.com/bkeepers/dotenv#what-other-env-files-can-i-use
  const dotenvFiles = [
    `${dotenvPath}.${NODE_ENV}.local`,
    // Don't include `.env.local` for `test` environment
    // since normally you expect tests to produce the same
    // results for everyone
    NODE_ENV !== 'test' && `${dotenvPath}.local`,
    `${dotenvPath}.${NODE_ENV}`,
    dotenvPath,
  ].filter(Boolean);

  // Load environment variables from .env* files. Suppress warnings using silent
  // if this file is missing. dotenv will never modify any environment variables
  // that have already been set.  Variable expansion is supported in .env files.
  // https://github.com/motdotla/dotenv
  // https://github.com/motdotla/dotenv-expand
  dotenvFiles.forEach(dotenvFile => {
    if (dotenvFile && fs.existsSync(dotenvFile)) {
      // eslint-disable-next-line
      require('dotenv-expand')(
        // eslint-disable-next-line
        require('dotenv-safe').config({
          allowEmptyValues: true,
          path: dotenvFile,
        }),
      );
    }
  });

  const projectNameRegex = new RegExp(`^${projectName}_`, 'i');

  const config = Object.keys(process.env)
    .filter(key => projectNameRegex.test(key))
    .reduce((env, key) => ({ ...env, [key]: process.env[key] }), {
      NODE_ENV,
      isTest: process.env.isTest,
    });

  const define = Object.entries(config).reduce(
    (env, [k, v]) => ({
      ...env,
      [`process.env.${k}`]: v,
    }),
    {},
  );

  console.log({ define });

  return {
    config,
    define,
  };
};
