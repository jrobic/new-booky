#!/usr/bin/env node

// eslint-disable-next-line import/no-extraneous-dependencies
const { build } = require('estrella');
const path = require('path');
const fs = require('fs');

const { config } = require('./env')();

const isProd = process.env.NODE_ENV === 'production';

const appDirectory = fs.realpathSync(process.cwd());
const resolveApp = relativePath => path.resolve(appDirectory, relativePath);

build({
  entry: resolveApp('src/main.ts'),
  outfile: isProd ? resolveApp('dist/main.js') : resolveApp('.esbuild/main.js'),
  target: 'es2020',
  platform: 'node',
  format: 'cjs',
  bundle: true,
  sourcemap: !isProd,
  debug: !isProd,
  tsconfig: resolveApp('tsconfig.json'),
  define: {
    'process.env.config': JSON.stringify(config),
  },
  run: isProd ? false : ['node', resolveApp('.esbuild/main.js')],
});
