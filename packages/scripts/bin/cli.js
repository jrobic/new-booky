#!/usr/bin/env node

// eslint-disable-next-line import/no-extraneous-dependencies
const spawn = require('cross-spawn');

process.on('unhandledRejection', err => {
  throw err;
});

const script = process.argv[2];
const args = process.argv.slice(3);

switch (script) {
  case 'build':
  case 'start': {
    const result = spawn.sync(process.execPath, [require.resolve('../src/build')].concat(args), {
      stdio: 'inherit',
    });

    process.exit(result.status);
    break;
  }
  default: {
    // eslint-disable-next-line no-console
    console.log(`Unknown script "${script}".`);
    break;
  }
}
