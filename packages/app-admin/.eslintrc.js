const path = require('path');

module.exports = {
  extends: ['../../.eslintrc', 'plugin:tailwindcss/recommended'],
  plugins: ['tailwindcss'],
  root: true,
  rules: {},
  settings: {
    tailwindcss: {
      config: path.join(__dirname, '/tailwind.config.js'),
    },
  },
};
