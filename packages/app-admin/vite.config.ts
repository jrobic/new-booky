import { defineConfig } from 'vite';
import reactRefresh from '@vitejs/plugin-react-refresh';
import tsconfigPaths from 'vite-tsconfig-paths';
// import {VitePWA} from 'vite-plugin-pwa'
import svgrPlugin from 'vite-plugin-svgr';
import env from 'vite-plugin-env-compatible';
import { visualizer } from 'rollup-plugin-visualizer';

/**
 * @type {import('vite').UserConfig}
 */
// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    reactRefresh(),
    tsconfigPaths(),
    svgrPlugin(),
    visualizer(),
    env({ prefix: 'SUPERVAN' }),
  ],
  css: {
    modules: {
      localsConvention: 'camelCaseOnly',
    },
  },
  server: {
    port: 9001,
  },
});
