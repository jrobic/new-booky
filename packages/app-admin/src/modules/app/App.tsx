import React from 'react';

import { AppLoader } from './AppLoader';

export const App = (): React.ReactElement => {
  return <AppLoader />;
};
