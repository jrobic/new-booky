import React from 'react';

export function AppLoader(): React.ReactElement {
  return (
    <div className="flex absolute inset-y-0 inset-x-0 z-50 justify-center bg-white bg-opacity-95">
      <div className="flex relative justify-center items-end h-1/3">
        <h1 className="text-6xl text-blue-500">BOOKY</h1>
        <span className="absolute right-3 -bottom-3.5 text-3xl animate-pulse">LOADING</span>
      </div>
    </div>
  );
}
