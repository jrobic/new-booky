const path = require('path');
const formsPlugin = require('@tailwindcss/forms');
const typographyPlugin = require('@tailwindcss/typography');

const fromRoot = p => path.join(__dirname, p);

module.exports = {
  // the NODE_ENV thing is for https://github.com/Acidic9/prettier-plugin-tailwind/issues/29
  mode: process.env.NODE_ENV ? 'jit' : undefined,
  purge: {
    mode: 'layers',
    options: {
      safelist: [],
    },
    enabled: process.env.NODE_ENV === 'production',
    content: ['index.html', fromRoot('./src/**/*.+(js|ts|tsx|mdx|md)')],
  },
  darkMode: 'class', // or 'media' or 'class'
  theme: {
    extend: {
      animation: {
        spooky: 'spooky 2s alternate infinite linear',
      },
      keyframes: {
        spooky: {
          '0%': {
            transform: 'translatey(.15em) scaley(.95)',
          },
          '100%': { transform: 'translatey(-.15em)' },
        },
      },
    },
  },
  variants: {
    extend: {
      opacity: ['disabled'],
      transitionProperty: ['responsive', 'motion-safe', 'motion-reduce'],
    },
  },
  plugins: [formsPlugin, typographyPlugin],
};
