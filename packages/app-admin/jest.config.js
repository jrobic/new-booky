const packageName = require('./package.json').name.replace(/@\w+\//, '');
const client = require('../../config/test/jest.app')(packageName);

module.exports = {
  ...client,
  moduleNameMapper: {
    ...client.moduleNameMapper,
    '^react-native$': 'react-native-web',
    '^.+\\.module\\.(css|sass|scss)$': 'identity-obj-proxy',
  },
  moduleFileExtensions: [
    'web.js',
    'js',
    'web.ts',
    'ts',
    'web.tsx',
    'tsx',
    'json',
    'web.jsx',
    'jsx',
    'node',
  ],
};
