const common = require('./config/test/jest.common');

process.env.BABEL_ENV = 'test';
process.env.NODE_ENV = 'test';
process.env.PUBLIC_URL = '';

module.exports = {
  ...common,
  roots: null,
  coverageDirectory: '<rootDir>/coverage',
  collectCoverageFrom: [
    '<rootDir>/**/*/src/**/*.{ts,tsx}',
    '!<rootDir>/**/*/src/generated.{ts,tsx}',
    '!<rootDir>/**/*/src/tests/*',
    '!<rootDir>/**/*/src/**/*.d.ts',
    '!<rootDir>/**/*/src/**/*.stories.*',
    '!<rootDir>/**/*/src/**/types/*',
    '!<rootDir>/**/*/src/**/*.types.*',
    '!<rootDir>/**/*/src/**/*.cy.spec.*',
  ],
  coverageReporters: ['json', 'lcov', 'text', 'clover', 'cobertura'],
  // coverageThreshold: {
  //   global: {
  //     statements: 80,
  //     branches: 70,
  //     functions: 70,
  //     lines: 80,
  //   },
  // },
  testURL: 'http://localhost/',
  projects: ['<rootDir>/packages/*/jest.config.js'],
};
