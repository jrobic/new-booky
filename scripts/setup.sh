#!/usr/bin/env sh
set -e

COLOR_DEFAULT='\033[0m'
COLOR_LCYAN='\033[1;36m'
COLOR_GREEN='\033[1;32m'

logInfo() {
  echo " "
  echo "\n${COLOR_GREEN}   $1 👍${COLOR_DEFAULT}\n"
  echo " "
}

echo "----------------------------------------------------------"
echo "\n*** ${COLOR_LCYAN}SETUP BOOKY${COLOR_DEFAULT} ***\n"
echo "----------------------------------------------------------"

npm i -g pnpm

pnpm i -r
logInfo "Dependances installed"

echo "----------------------------------------------------------"
echo "\n${COLOR_LCYAN}🎉 Setup complete! 🎉${COLOR_DEFAULT}\n"
echo "----------------------------------------------------------"
