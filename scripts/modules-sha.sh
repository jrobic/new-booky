#!/usr/bin/env sh

echo "----------------------------------------------------"
echo "Generate modules-sha"
echo "----------------------------------------------------"

find . -type d -name 'node_modules' -prune -false -o -name '.webpack' -prune -false -o -name '.history' -prune -false -o -type f -name 'pnpm-lock.yaml' | sort | xargs sha1sum >modules-sha && git add modules-sha
