// eslint-disable-next-line import/no-extraneous-dependencies
const fastGlob = require('fast-glob');
const fs = require('fs');

const appDirectory = fs.realpathSync(process.cwd());

const dotenvFiles = fastGlob.sync(`${appDirectory}/packages/{apps,services,utils}/**/.env.test`);

// Load environment variables from .env* files. Suppress warnings using silent
// if this file is missing. dotenv will never modify any environment variables
// that have already been set.  Variable expansion is supported in .env files.
// https://github.com/motdotla/dotenv
// https://github.com/motdotla/dotenv-expand
dotenvFiles.forEach(dotenvFile => {
  if (dotenvFile && fs.existsSync(dotenvFile)) {
    // eslint-disable-next-line
    require('dotenv-expand')(
      // eslint-disable-next-line
      require('dotenv').config({
        allowEmptyValues: true,
        path: dotenvFile,
      }),
    );
  }
});
