const path = require('path');
const hq = require('alias-hq');

module.exports = {
  testEnvironment: 'node',
  rootDir: path.join(__dirname, '../..'),
  resetMocks: true,
  globalSetup: '<rootDir>/config/test/globalSetup.js',
  setupFiles: ['<rootDir>/config/test/loadEnv.js'],

  transformIgnorePatterns: ['[/\\\\]node_modules[/\\\\].+\\.tsx?$'],
  transform: {
    '^.+\\.(ts|tsx)$': [
      'esbuild-jest',
      {
        sourcemap: true,
      },
    ],
  },
  moduleDirectories: ['node_modules'],

  watchPlugins: [
    'jest-watch-typeahead/filename',
    'jest-watch-typeahead/testname',
    'jest-watch-select-projects',
  ],

  moduleNameMapper: { ...hq.get('jest') },
};
