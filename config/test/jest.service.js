const baseConfig = require('./jest.common');

module.exports = (packageName = 'server') => ({
  ...baseConfig,
  testEnvironment: 'jest-environment-node',
  setupFilesAfterEnv: [`<rootDir>/packages/${packageName}/src/setupTests.ts`],
  snapshotSerializers: [],
  testRegex: `(packages/${packageName}/.*/src/.*|\\.(test|spec|e2e))\\.tsx?$`,
  testURL: 'http://localhost/',

  moduleDirectories: ['node_modules'],
  modulePaths: [`<rootDir>/packages/${packageName}/src/`],
  name: packageName,
  displayName: packageName,
  roots: [`<rootDir>/packages/${packageName}`],
});
