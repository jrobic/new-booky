const hq = require('alias-hq');
const baseConfig = require('./jest.common');

module.exports = (packageName = 'client') => ({
  ...baseConfig,
  testEnvironment: 'jest-environment-jsdom',
  setupFilesAfterEnv: [`<rootDir>/packages/${packageName}/src/setupTests.ts`],
  snapshotSerializers: [],
  testRegex: `(packages/${packageName}/.*/src/.*|\\.(test|spec|e2e))\\.tsx?$`,
  testURL: 'http://localhost/',
  moduleNameMapper: {
    ...hq.get('jest'),
    '\\.module\\.css$': 'identity-obj-proxy',
    '\\.css$': require.resolve('./style-mock.js'),
  },
  moduleDirectories: ['node_modules'],
  modulePaths: [`<rootDir>/packages/${packageName}/src/`],
  name: packageName,
  displayName: packageName,
  roots: [`<rootDir>/packages/${packageName}`],
});
