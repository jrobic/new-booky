module.exports = async () => {
  process.env.TZ = process.env.TZ || 'UTC';
  process.env.isTest = true;
};
